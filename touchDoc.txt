HTML5 bring a new elemnet: TOUCH Event

Attribute:
-identifier:	unique numeric identifier
-target:	the DOM element that is the event target of the touch, even if the touch has moved outside of this element
-screenX:	horizontal coordinate relative to screen
-screenY:	vertical coordinate relative to screen
-clientX:	horizontal coordinate of point relative to viewport, excluding scroll offset
-clientY:	vertical coordinate of point relative to viewport, excluding scroll offset
-pageX:	horizontal coordinate of point relative to viewport, including scroll offset
-pageY:	vertical coordinate of point relative to viewport, including scroll offset

Events:
-touchstart:	triggered when a touch is detected
-touchmove:	triggered when a touch movement is detected
-touchend:	triggered when a touch is removed e.g. the user’s finger is removed from the touchscreen
-touchcancel:	triggered when a touch is interrupted, e.g. if touch moves outside of the touch-capable area

List(associated with each event):
-touches:	all current touches on the screen
-targetTouches:	all current touches that started on the target element of the event
-changedTouches:	all touches involved in the current event